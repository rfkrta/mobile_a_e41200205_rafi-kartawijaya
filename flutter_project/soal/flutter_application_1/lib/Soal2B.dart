import 'dart:io';

void main() {
  print("Masukkan Nama Anda :  ");
  var name = stdin.readLineSync()!;

  if (name == "") {
    print("Nama Harus diisi !!");
  } else {
    print("Pilih peran Penyihir | Guard | Werewolf ");
    var peran = stdin.readLineSync()!;

    if (peran == "") {
      print("Halo " + name + " Pilih peranmu untuk Memulai game");
    } else if (peran == "Penyihir") {
      print("Selamat datang di Dunia Werewolf, " +
          name +
          ". Halo Penyihir " +
          name +
          " Kamu dapat melihat siapa yang menjadi werewolf");
    } else if (peran == "Guard") {
      print("Selamat datang di Dunia Werewolf, " +
          name +
          ". Halo Guard " +
          name +
          " Kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peran == "Werewolf") {
      print("Selamat datang di Dunia Werewolf, " +
          name +
          ". Halo Werewolf " +
          name +
          " Kamu akan memangsa setiap malam");
    }
  }
}
