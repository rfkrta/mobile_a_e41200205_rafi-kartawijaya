void main(List<String> args) {
  print(tampilkan());
  print(muncukanangka());
  print(kalikanDua(5));
  print(kalikan(5, 3));
  print(tampilkanangka(5));
}

tampilkan(){
  print("Hello Peserta Bootcamp");
}

muncukanangka(){
  return 2;
}

kalikanDua(angka){
  return angka * 2;
}

kalikan(x, y){
  return x * y;
}

tampilkanangka(n1,{s1:45}){
  print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
  print(s1); //hasil adalah 45 karena dari parameter diisi 45
}