import 'bangun_datar.dart';

class segitiga extends bangun_datar {
  double a = 0;
  double b = 0;
  double c = 0;
  double alas = 0;
  double tinggi = 0;

  segitiga (double a, double b, double c,double alas, double tinggi){
    this.a = a;
    this.b = b;
    this.c = c;
    this.alas = alas;
    this.tinggi = tinggi;
  }

  @override
  double luas(){
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling(){
    return a*b*c;
  }
}