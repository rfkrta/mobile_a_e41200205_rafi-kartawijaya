import 'bangun_datar.dart';

class persegi extends bangun_datar {
  double s = 0;

  persegi (double s){
    this.s = s;
  }

  @override 
  double luas(){
    return s*s;
  }
  @override
  double keliling(){
    return s*4;
  }
}