import 'dart:io';

void main() {
  print("Apakah anda ingin menginstall aplikasi ini? (Y/T)");
  var inputText = stdin.readLineSync()!;
  inputText == "Y"
      ? print("Anda akan menginstall aplikasi dart")
      : inputText == "T"
          ? print("Aborted")
          : print("");
}
